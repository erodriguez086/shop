package domain;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Date;
import javax.swing.text.html.HTMLDocument.Iterator;


public class PurchaseTest {

	private Purchase basket=new Purchase();
	private Date data = new Date();

	@Test
	public void testAddBasket1() {
		int stock=5;
		float price= (float) 2.5;
		int quantity=1;
		//1. expected value
		double expectedResult = quantity * price;
		//2. invoke the method and get the result
		basket = new Purchase();
		Article art=new Article("1", "16Gb pendrive", price, false, stock);
		double result = basket.addBasket(art,quantity);
		//3. check
		assertEquals(expectedResult,result);
	}
	@Test
	public void testAddBasket2() {
		int stock=5;
		float price=(float) 2.5;
		int quantity=-1; //negatiboa kenduta
		//1. expected value
		double expectedResult = 0;
		//2. invoke the method and get the result
		basket = new Purchase();
		Article art=new Article("1","16Gb pendrive", price, false, stock);
		double result = basket.addBasket(art,quantity);
		//3. check
		assertEquals(expectedResult,result);
	}
	@Test
	public void testAddBasket3() {
		int stock=5;
		float price=(float) 2.5;
		int quantity=1;
		//1. expected value
		//ERROREA
		//2. invoke the method and get the result
		basket = new Purchase();
		basket.setPurchasedDate(data);
		Article art=new Article("1","16Gb pendrive", price, false, stock);
		//3. check
		try {
			double result = basket.addBasket(art,quantity);
			fail();
		} catch (RuntimeException e) {
			assertTrue(true);
		}
	}
	@Test
	public void testAddBasket4() {
		int stock=5;
		float price=(float) 2.5;
		int quantity=1;
		//1. expected value
		double expectedResult = (float)2.5;
		//2. invoke the method and get the result
		basket = new Purchase();
		Article art=new Article("1","16Gb pendrive",price, false, stock);
		PurchasedArticle part=new PurchasedArticle(art,	quantity);
		basket.getBasket().put(art, part);
		double result = basket.addBasket(art,quantity);
		//3. check
		assertEquals(expectedResult,result);
	}
	@Test
	public void testAddBasket5() {
		int stock=5;
		float price=(float) 2.5;
		int quantity=8;
		//1. expected value
		//2. invoke the method and get the result
		basket = new Purchase();
		Article art=new Article("1", "16Gb pendrive", price, false, stock);
		//3. check
		try {
			double result = basket.addBasket(art,quantity);
			fail();
		} catch (RuntimeException e) {
			assertTrue(true);
		}
	}

	@Test
	public void testRemoveBasket1() {
		int stock=5;
		float price= (float) 2.5;
		int quantity=1;
		//1. expected value
		//ERROREA
		//2. invoke the method and get the result
		basket = new Purchase();
		basket.setPurchasedDate(data);
		Article art=new Article("1","16Gb pendrive", price, false, stock);
		//3. check
		try {
			double result = basket.removeBasket(art,quantity);
			fail();
		} catch (RuntimeException e) {
			assertTrue(true);
		}

	}
	@Test
	public void testRemoveBasket2() {
		int stock=5;
		float price=(float) 2.5;
		int quantity=1;
		//1. expected value
		//ERROREA
		//2. invoke the method and get the result
		basket = new Purchase();
		Article art=new Article("1", "16Gb pendrive", price, false, stock);
		try {
			double result = basket.removeBasket(art,quantity);
			fail();
		}catch (RuntimeException e){
			assertTrue(true);
		}




	}
	@Test
	public void testRemoveBasket3() {
		int stock=5;
		float price=(float) 2.5;
		int quantity=1;
		int rQuantity=3;
		//1. expected value
		double expectedResult = quantity * price;
		//2. invoke the method and get the result
		basket = new Purchase();
		Article art=new Article("1", "16Gb pendrive", price, false, stock);
		basket.addBasket(art,quantity);
		//3.check
		try {
			double result = basket.removeBasket(art,rQuantity);
			fail();
		}catch (RuntimeException e){
			assertTrue(true);
		}
	}
	@Test
	public void testRemoveBasket4() {
		int stock=5;
		float price=(float) 2.5;
		int quantity=3;
		int rQuantity=3;
		//1. expected value
		double expectedResult = 0;
		//2. invoke the method and get the result
		basket = new Purchase();
		Article art=new Article("1", "16Gb pendrive", price, false, stock);
		basket.addBasket(art,quantity);
		double result = basket.removeBasket(art,rQuantity);
		//3.check
		assertEquals(expectedResult,result);
	}
	@Test
	public void testRemoveBasket5() {
		int stock=5;
		float price=(float) 2.5;
		int quantity=4;
		int rQuantity=3;
		//1. expected value
		double expectedResult = (quantity-rQuantity)*price;
		//2. invoke the method and get the result
		basket = new Purchase();
		Article art=new Article("1", "16Gb pendrive", price, false, stock);
		basket.addBasket(art,quantity);
		double result = basket.removeBasket(art,rQuantity);
		//3.check
		assertEquals(expectedResult,result);
	}

}